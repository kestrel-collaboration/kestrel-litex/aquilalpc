#!/usr/bin/env python3

# This file is Copyright (c) 2018-2019 Florent Kermarrec <florent@enjoy-digital.fr>
# This file is Copyright (c) 2018-2019 David Shah <dave@ds0.me>
# This file is Copyright (c) 2020-2022 Raptor Engineering, LLC
# License: BSD

import os
import argparse
import subprocess
import tempfile

from migen import *

from litex import get_data_mod

from litex.soc.interconnect import wishbone, stream
from litex.soc.interconnect.csr import *
from litex.gen.common import reverse_bytes
from litex.build.io import SDRTristate

kB = 1024
mB = 1024*kB

# LPC Slave interface ------------------------------------------------------------------------------

from litex.soc.interconnect.csr_eventmanager import EventManager, EventSourceLevel
from litex.soc.interconnect import wishbone, stream
from litex.gen.common import reverse_bytes
from litex.build.io import SDRTristate

class AquilaLPCSlave(Module, AutoCSR):
    def __init__(self, platform, pads, clk_freq, endianness="big", adr_offset=0x0, debug_signals=None, lpc_clk_mirror=None):
        self.slave_bus    = slave_bus    = wishbone.Interface(data_width=32, adr_width=30)
        self.master_bus   = master_bus   = wishbone.Interface(data_width=32, adr_width=30)
        self.wb_irq = Signal()

        # Set up IRQ handling
        self.submodules.ev = EventManager()
        self.ev.master_interrupt = EventSourceLevel(name="IRQ", description="Aquila master interrupt, asserted for any active interrupt condition in the slave core")
        self.ev.finalize()

        # Bus endianness handlers
        self.slave_dat_w = Signal(32)
        self.slave_dat_r = Signal(32)
        self.comb += self.slave_dat_w.eq(slave_bus.dat_w if endianness == "big" else reverse_bytes(slave_bus.dat_w))
        self.comb += slave_bus.dat_r.eq(self.slave_dat_r if endianness == "big" else reverse_bytes(self.slave_dat_r))

        self.master_dat_w = Signal(32)
        self.master_dat_r = Signal(32)
        self.comb += self.master_dat_r.eq(master_bus.dat_r if endianness == "big" else reverse_bytes(master_bus.dat_r))
        self.comb += master_bus.dat_w.eq(self.master_dat_w if endianness == "big" else reverse_bytes(self.master_dat_w))

        # Calculate slave address
        self.slave_address = Signal(30)
        self.comb += self.slave_address.eq(slave_bus.adr - (adr_offset >> 2)) # wb address is in words, offset is in bytes

        # Debug signals
        self.debug_port = Signal(16)
        self.lpc_clock_mirror = Signal()

        # LPC interface signals
        self.lpc_data_out = Signal(4)
        self.lpc_data_in = Signal(4)
        self.lpc_data_direction = Signal()
        self.lpc_irq_out = Signal()
        self.lpc_irq_in = Signal()
        self.lpc_irq_direction = Signal()
        self.lpc_frame_n = Signal()
        self.lpc_reset_n = Signal()
        self.lpc_clock = Signal()

        self.specials += Instance("aquila_lpc_slave_wishbone",
            # Configuration data
            p_WISHBONE_BUS_FREQUENCY_HZ = clk_freq,

            # Wishbone slave port signals
            i_slave_wb_cyc = slave_bus.cyc,
            i_slave_wb_stb = slave_bus.stb,
            i_slave_wb_we = slave_bus.we,
            i_slave_wb_addr = self.slave_address,
            i_slave_wb_dat_w = self.slave_dat_w,
            o_slave_wb_dat_r = self.slave_dat_r,
            i_slave_wb_sel = slave_bus.sel,
            o_slave_wb_ack = slave_bus.ack,
            o_slave_wb_err = slave_bus.err,
            o_slave_irq_o = self.wb_irq,

            # Wishbone master port signals
            o_master_wb_cyc = master_bus.cyc,
            o_master_wb_stb = master_bus.stb,
            o_master_wb_we = master_bus.we,
            o_master_wb_addr = master_bus.adr,
            o_master_wb_dat_w = self.master_dat_w,
            i_master_wb_dat_r = self.master_dat_r,
            o_master_wb_sel = master_bus.sel,
            i_master_wb_ack = master_bus.ack,
            i_master_wb_err = master_bus.err,

            # LPC core signals
            o_lpc_data_out = self.lpc_data_out,
            i_lpc_data_in = self.lpc_data_in,
            o_lpc_data_direction = self.lpc_data_direction,
            o_lpc_irq_out = self.lpc_irq_out,
            i_lpc_irq_in = self.lpc_irq_in,
            o_lpc_irq_direction = self.lpc_irq_direction,
            i_lpc_frame_n = self.lpc_frame_n,
            i_lpc_reset_n = self.lpc_reset_n,
            i_lpc_clock = self.lpc_clock,

            # Clock and reset
            # Put the peripheral on the both main system clock and reset domains
            i_peripheral_reset = ResetSignal('sys'),
            i_peripheral_clock = ClockSignal('sys'),

            # Debug port
            o_debug_port = self.debug_port,
            o_lpc_clock_mirror = self.lpc_clock_mirror
        )
        # Add Verilog source files
        self.add_sources(platform)

        # I/O drivers
        self.specials += SDRTristate(
            io  = pads.addrdata[0],
            o   = self.lpc_data_out[0],
            oe  = self.lpc_data_direction,
            i   = self.lpc_data_in[0],
            clk = self.lpc_clock_mirror,
        )
        self.specials += SDRTristate(
            io  = pads.addrdata[1],
            o   = self.lpc_data_out[1],
            oe  = self.lpc_data_direction,
            i   = self.lpc_data_in[1],
            clk = self.lpc_clock_mirror,
        )
        self.specials += SDRTristate(
            io  = pads.addrdata[2],
            o   = self.lpc_data_out[2],
            oe  = self.lpc_data_direction,
            i   = self.lpc_data_in[2],
            clk = self.lpc_clock_mirror,
        )
        self.specials += SDRTristate(
            io  = pads.addrdata[3],
            o   = self.lpc_data_out[3],
            oe  = self.lpc_data_direction,
            i   = self.lpc_data_in[3],
            clk = self.lpc_clock_mirror,
        )
        self.specials += SDRTristate(
            io  = pads.serirq,
            o   = self.lpc_irq_out,
            oe  = self.lpc_irq_direction,
            i   = self.lpc_irq_in,
        )
        self.comb += self.lpc_frame_n.eq(pads.frame_n)
        self.comb += self.lpc_reset_n.eq(pads.reset_n)
        self.comb += self.lpc_clock.eq(pads.clk)

        if debug_signals is not None:
            self.comb += debug_signals[0].eq(self.debug_port[15])
            self.comb += debug_signals[1].eq(self.debug_port[14])
            self.comb += debug_signals[2].eq(self.debug_port[13])
            self.comb += debug_signals[3].eq(self.debug_port[12])
            self.comb += debug_signals[4].eq(self.debug_port[11])
            self.comb += debug_signals[5].eq(self.debug_port[10])
            self.comb += debug_signals[6].eq(self.debug_port[9])
            self.comb += debug_signals[7].eq(self.debug_port[8])

        if lpc_clk_mirror is not None:
            self.comb += lpc_clk_mirror.eq(self.lpc_clock_mirror)

        # Generate IRQ
        # Active high logic
        self.comb += self.ev.master_interrupt.trigger.eq(self.wb_irq)


    @staticmethod
    def add_sources(platform):
        vdir = get_data_mod("peripheral", "aquilalpc").data_location
        platform.add_source(os.path.join(vdir, "wishbone_interface.v"))
        platform.add_source(os.path.join(vdir, "lpc_slave.v"))
        platform.add_source(os.path.join(vdir, "lpc_master.v"))
